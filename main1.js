var canvas = document.getElementById("Canvas")
var context = canvas.getContext('2d')


canvas.width = window.innerWidth
canvas.height = window.innerHeight

//Without Constructor: Run
var spriteArray = [];
spriteArray.length = 10;

for(var imgCount = 0; imgCount < spriteArray.length; imgCount++)
{
    spriteArray[imgCount] = new Image();
    spriteArray[imgCount].src = "Run/Run__00"+imgCount.toString()+".png";
    //spriteArray[imgCount].src = "ZombieWalk/Walk_00"+imgCount.toString()+".png";
    
}
var i = 0;
setInterval(function()
{   
    if(i > 9)
    {
        i = 0;
    }
    console.log(i)
    context.clearRect(0,0, canvas.width,canvas.height);
    //context.clearRect(100,100,100,100);
    context.drawImage(spriteArray[i],100,canvas.height-200,150,200);
    i++;

}, 50);