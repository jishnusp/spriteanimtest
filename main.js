var canvas = document.getElementById("Canvas")
var context = canvas.getContext("2d")


canvas.width = window.innerWidth
canvas.height = window.innerHeight
//With Constructor: Run
function SpriteAnimation(length, path)
{
    this.spriteArray = [];
    this.spriteArray.length = length;

    for(var imgCount = 0; imgCount < this.spriteArray.length; imgCount++)
    {
        this.spriteArray[imgCount] = new Image();
        this.spriteArray[imgCount].src =path + imgCount.toString() + ".png";
       
    }
}

var runPlayer = new SpriteAnimation(10, "Run/Run__00");

var i = 0;
setInterval(function()
{
    if (i < runPlayer.spriteArray.length)
    {
        console.log(runPlayer.spriteArray)
        context.clearRect(0,0, canvas.width,canvas.height);
        //context.drawImage(runPlayer.spriteArray[i],100,100,100,100);
        context.drawImage(runPlayer.spriteArray[i],100,canvas.height-200,150,200);
        i++;
    }
    else
    {
        i = 0;
    }

}, 50);